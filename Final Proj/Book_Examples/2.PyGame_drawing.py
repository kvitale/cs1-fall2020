import pygame, sys
from pygame.locals import *

pygame.init()

DISPLAYSURF = pygame.display.set_mode((500, 400))
pygame.display.set_caption("Drawing")

# set up colors

Black = (0, 0, 0)
White = (255, 255, 255)
Red = (255, 0, 0)
Green = (0, 255, 0)
Blue = (0, 0, 255)

# draw on surface object
DISPLAYSURF.fill(White)
pygame.draw.polygon(DISPLAYSURF, Green, ((146, 0), (291, 106),(236, 277), (56, 277), (0, 106)))
pygame.draw.line(DISPLAYSURF, Blue, (0, 0), (120, 60), 2)
pygame.draw.line(DISPLAYSURF, Blue, (120, 60), (60, 120))
pygame.draw.line(DISPLAYSURF, Blue, (60, 120), (120, 400), 2)
pygame.draw.circle(DISPLAYSURF, Blue, (150, 150), 20    )
pygame.draw.ellipse(DISPLAYSURF, Red, (300, 250, 40, 100))
pygame.draw.rect(DISPLAYSURF, Red, (100, 150, 150, 50))

pixObj = pygame.PixelArray(DISPLAYSURF)
pixObj[480][380] = Black
pixObj[482][382] = Black
pixObj[484][384] = Black
pixObj[486][386] = Black
pixObj[488][388] = Black
del pixObj

# run game loop
while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
    
