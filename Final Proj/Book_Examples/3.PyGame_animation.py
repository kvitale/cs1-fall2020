import pygame, sys
from pygame.locals import *

pygame.init()

FPS = 30 #frames per second
fpsClock = pygame.time.Clock()

#set up Window
DISPLAYSURF = pygame.display.set_mode((400, 300), 0, 32)
pygame.display.set_caption('Animation')

White = (255, 255, 255)
catX = 10
catY = 10

catImg = pygame.image.load('cat.png')
catRect = pygame.Rect(catX, catY, 100,100)

cherry = pygame.image.load('cherry.png')
direction = 'right'

while True: # main game loop
    DISPLAYSURF.fill(White)

    if direction == 'right':
        catRect.x += 5
        if catRect.x == 280:
            direction = 'down'
    elif direction == 'down':
        catRect.y += 5
        if catRect.y == 220:
            direction = 'left'
    elif direction == 'left':
        catRect.x -= 5
        if catRect.x == 10:
           direction = 'up'
    elif direction == 'up':
        catRect.y -= 5
        if catRect.y == 10:
           direction = 'right'

    DISPLAYSURF.blit(catImg, catRect)

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    pygame.display.update()
    fpsClock.tick(FPS)
