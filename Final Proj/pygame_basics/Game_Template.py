# Pygame template for learning and experimentation
# This interactive program uses pygame to create a drawing board with four different brushes, which allow you to draw with
# shapes, text and images.

# Based on templates from James D. Miller; Gustavus Adolphus College and from the source code for "Making Games with Pygame"
# Computer Science 1 with Sachi DeCou, Kyle Vitale

############################################
############################################

# import pygame and sys modules
import pygame
import sys

# import all local variables from pygame
from pygame.locals import *

# set up pygame
# Pygame library requires this initial code to be run after importing pygame module and before calling Pygame functions
pygame.init()

# set up a display window called window_surface
# there are three parameters to the set_mode() method which is used when creating a window where your
# pygame creations will appear:
# 1. a tuple of two integers for the width and height of the window, in pixels
# 2. for advanced GUI (Graphic User Interface) window options. You won't need this so set it to 0.
# 3. Advanced option for color depth. For our purposes pass it the value of 32
window_surface = pygame.display.set_mode((500, 500), 0, 32)

# Set a caption for the window you've created
pygame.display.set_caption('Play')

# Set up colors: assign variable names to color values as 3 integer tuples representing the RGB value for each color.
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
RED = (255, 0, 0)
AQUA = (0, 255, 255)
FUCSHIA = (255, 0, 255)
MAROON = (128, 0, 0)
YELLOW = (255, 255, 0)
GREEN= (0, 255, 0)
WHITE=(255, 255, 255)
GREY = (120, 120, 120)

# Set the initial color of the whole pygame window.
window_surface.fill(WHITE)

# Initialize variables that will be used to control various aspects of the program
#framerate_limit = 120
time_s = 0.0
key_e = "U"
key_f = "U"
key_c = 'U'
key_t = 'U'
key_r = 'U'
key_k = 'U'
play_active = True
mouse_button_UpDown = "U"
brush_color = YELLOW
font_size = 40
text_draw = "blue"

# loads an image into the program
cat_image = pygame.image.load('cat.png')

# loads a sound into the program
pygame.mixer.music.load('Cat_Meow_2-Cat_Stevens-2034822903.mp3')


# =====================================
# Main Game Loop
# =====================================
# The game loop is a loop that constantly checks for new events, updates the state of the window,
# and draws the current version of the window to the screen.

while play_active:

    # =====================================================
    # Begin user input collection
    # =====================================================

    # loop through the list of events in the event queue.
    for event in pygame.event.get():

        # This main "if" structure checks the event type of each event.
        # Depending on the event type (QUIT, KEYDOWN, KEYUP, MOUSEBUTTONDOWN, or
        # MOUSEBUTTONUP), additional checks are made to identify the characteristics of the
        # event.
        if (event.type == pygame.QUIT):
            play_active = False
            sys.exit()

        elif (event.type == pygame.KEYDOWN):
            if (event.key == K_ESCAPE):
                play_active = False
                sys.exit()
            elif (event.key == K_e):
                key_e = 'D'
            elif (event.key == K_f):
                key_f = 'D'
            elif (event.key == K_t):
                key_t = 'D'
            elif (event.key == K_c):
                key_c = 'D'
            elif (event.key == K_r):
                key_r = 'D'
            elif (event.key == K_k):
                key_k = 'D'

        elif (event.type == pygame.KEYUP):
            if (event.key == K_e):
                key_e = 'U'
            elif (event.key == K_f):
                key_f = 'U'
            elif (event.key == K_t):
                key_t = 'U'
            elif (event.key == K_c):
                key_c = 'U'
            elif (event.key == K_r):
                key_r = 'U'
            elif (event.key == K_k):
                key_k = 'U'

        elif (event.type == pygame.MOUSEBUTTONDOWN):
            mouse_button_UpDown = 'D'

            # The get_pressed method returns True/False values in a tuple.
            (button1, button2, button3) = pygame.mouse.get_pressed()

            if button1:
                mouse_button = 1
            elif button2:
                mouse_button = 2
            elif button3:
                mouse_button = 3
            else:
                mouse_button = 0

        elif (event.type == pygame.MOUSEBUTTONUP):
            mouse_button_UpDown = 'U'

    # Get the x, y position of the mouse cursor. Return this as a tuple.
    mouse_xy = pygame.mouse.get_pos()
    x, y = pygame.mouse.get_pos()

    # =====================================================
    # End of user input collection
    # =====================================================


    # =====================================================
    # Actions to execute based on user input
    # =====================================================

    # If the "d" key is pressed turn the background grey. Do this by filling the entire
    # screen with grey color.
    if (key_e == 'D'):
        window_surface.fill(GREY)


    # Define brush keys

    # Text Brush: if the "t" key is pressed draw with text
    if (key_t == 'D'):

        # write text to screen:
        # create pygame.font.Font object (aka Font object) by calling pygame.font.Sysfont() function with parameters:
        #   1. name of font and 2. size of font measured in points
        font_choice = pygame.font.SysFont("Courier", font_size, True, True)

        # call render method on Font object, font_choice, which creates a Surface object with the text drawn on it.
        # render() has four parameters: 1. a string of the desired text, 2. a boolean for aliasing or not, 3. text color, 4. bg color
        text = font_choice.render(text_draw, True, brush_color, None)

        # returns a rect object that represents the size and position of the Font or Surface object
        textRect = text.get_rect()

        # blit() method draws the contents of one Surface object onto another Surface object.
        # In this case the text object, text, is drawn onto the Surface object, windowSurface.
        # The blit() function has two parameters, the object to be drawn and the location to draw it onto.
        # positions the center of the rect object, textRect, at the x,y position of the mouse.
        textRect.centerx, textRect.centery = x, y
        window_surface.blit(text, textRect)

    # Rectangle Brush: if the "r" key is pressed draw with a rectangle shaped brush
    elif (key_r == 'D'):

        # draw the rectangle to the screen
        # create Rect object
        rect_1 = pygame.Rect(x, y, 30, 20)
        pygame.draw.rect(window_surface, brush_color, rect_1)

    # Image Brush: if the k is pressed draw with the image brush
    elif (key_k == 'D'):

        # blit() method draws the contents of one Surface object onto another Surface object.
        # The blit() function has two parameters, the object to be drawn and the location to draw it onto.
        # Here you are using it to draw the image loaded into the variable cat_image to the screen.
        window_surface.blit(cat_image, (x, y))

        # play the sound you've brought into the program via the pygame.mixer.music.load () command on line 63 once,
        # and starting 2.5 seconds from the beginning.
        pygame.mixer.music.play(0, 2.5)

    # Circle Brush: if no key is pressed draw with a circle
    else:

        #draw the circle to the screen
        pygame.draw.circle(window_surface, brush_color, mouse_xy, 10, 0)


    # Determine the color for the moving object, either circle, rectangle or text, based on if a mouse button is up (U) or down (D).
    # For the text brush this also defines the font size and the text to draw.
    if ((mouse_button_UpDown == 'D') and (mouse_button == 1)):
        brush_color = YELLOW
        font_size = 25
        text_draw = "yellow"
    elif ((mouse_button_UpDown == 'D') and (mouse_button == 3)):
        brush_color = RED
        font_size = 100
        text_draw = "red"
    else:
        brush_color = BLUE
        font_size = 40
        text_draw = "blue"


    # If the "f" key is up (not down), update the entire display window.
    if (key_f != 'D'):
        # draws the window onto the screen, must be called each time you want to update the display screen to display the contents
        # of the Surface object returned by pygame.display.set_mode()
        pygame.display.update()

    # =====================================================================
    # End of Game Loop - Loop repeats until user presses the "Esc" key or quits the program,
    # =====================================================================
