# Notes from Day 7
# Python Lists

myEmptyList = [4, 234, 'this', 'that' ]
myList = [4, 23.2, 'Hello', 'World', [32,234.2, 'embed'] ]

x = myList + myList + myList
y = 3 * myList
print(x)
print(y)

print(myList[0])
print(myList[3])
print(myList[2])
print(myList[-2])
print(myList[1:4])
print(myList[0:-1])

var1 = myList[0]
var2 = myList[4] + myList[4]

#print(var2)

#print(var1 + var2)
#print( len(myList) )
