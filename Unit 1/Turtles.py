# Some examples of using Turtles

import turtle
wn = turtle.Screen()  # This creates the graphics window that turtles draw on
alex = turtle.Turtle()  # This is a "turtle" object named alex.
meg = turtle.Turtle()

# Alex draws a square by repeating lines of code
alex.forward(80)  # This moves alex forward (whatever direction alex is facing) 80 pixels
alex.left(90)  # This turns alex left, 90 degrees.
alex.forward(80)
alex.left(90)
alex.forward(80)
alex.left(90)
alex.forward(80)
alex.left(90)

# Meg's square
meg.color('RED')
meg.penup()
meg.goto(-100,100)
meg.pendown()

for i in range(4):  # Repeat 4 times
    # Creates a square
    meg.forward(100)
    meg.left(90)

meg.penup()
meg.goto(200,-100)
meg.pendown()
meg.color('GREEN')


for i in range(4):  # Repeat 4 times
    meg.forward(40)
    meg.left(90)

meg.penup()
meg.goto(-100 , -100)
meg.pendown()
meg.color('BLUE')
meg.circle(100)

wn.exitonclick()

