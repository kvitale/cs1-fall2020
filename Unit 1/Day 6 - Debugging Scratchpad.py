# Day 6 - Debugging - Scratchpad

# Do one thing at time to minimize mistakes that cna quickly build and
# be difficult to find later.

# Use Print statements as a troubleshooting tool. Print variables at different
# times in a program to track how they may change. Do this a lot!

# Comment and uncomment lines instead of deleting them. It is hard to remember
# what was there, especially when every character in a line of code counts.

current_time = input("what is the current time (in hours)? ")
wait_time = input("How many hours do you want to wait: ")

print(current_time, ' ', type(current_time))
print(wait_time, ' ', type(current_time))

current_time_int = int(current_time)
wait_time_int = int(wait_time)




print(current_time_int, wait_time_int)

