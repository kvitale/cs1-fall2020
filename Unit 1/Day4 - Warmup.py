## Day4: Warmup, 8-30-2020

print("Hello, world!")
print("Hello", "world!")
print(3)
print(3.0)
print(2 + 3)
print(2.0 + 3.0)
print("2" + "3"),
print("2 + 3 =", 2 + 3)
print(2 * 3)
print(2 ** 3)
print(2 / 3)
print(4 // 3)
print(5 % 3)

