#Math and Random Modules

import math

print('Pi:', math.pi)
print('Degrees:', math.degrees(1))
print('Factorial:', math.factorial(10))

import random

#Random float
print('A random float:', random.random())

#Random integer
print('A Random integer:',random.randint(1,100))

someList = ['a', 'this', 'that', 1235, 54.7]
#Random item from a list
print('Random item from a list:', random.choice(someList))
