# Scratch pad for day 5, 9-1-2020

#expression, needs to be evaluated
print(4)
print(len('hello world'))

#statement, instructions python can execute
a = 4

#combination of statement and expression

b = 4 + int('12') #convert string '12' to the integer 12
print(b) #expect 16

import random

#input
answer = input('Enter your name: ') #Program stops and waits for a user to input something
age = input('Enter your age: ')

age = int(age) #Convert age to an integer

age = age + 1 #Now I can do math of the age variable.

print(answer)
print(age)

#Functions used to convert form one type to another
#int(), float(), str()

int(4.5) #convert a float to an int
float(4) #convert int to a float
int('12') #convert string to int
str(12) #convert to a string

#Single and double quotes are OK for creating strings
"this is a string"
'this is also a string'



