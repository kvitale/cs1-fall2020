# Day 5 Warmup, 9/1/2020

# Write the program that switches the values of two variables.
# We talked through the algorithm to solving this problem last class.


a = 4
b = 12
print('a=', a, 'b=', b)

print(a)
print('a')

# Do the switcheroo

c = b
b = a
a = c


print('a=' + str(a) + 'b=' + str(b))
