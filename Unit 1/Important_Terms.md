# Important Concepts to Keep in Mind

- Problem Solving

- Algorithms

- Programming Languages

- High/Low level languages

- Interpreters vs Compilers

- A Program

- Debugging

- Syntax / Runtime / Semantic Errors

- For Loops

- Operations

- Statements

- Assignment
