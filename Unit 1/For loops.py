# For Loops scratchpad

# example 1
for i in (1, 2, 3):
    print('Here we go:', i)  # this is the loop body, even though it has only one line
    print('This line is also part of the loop body, and should appear 3x')
    print('Here we go again:', i)
print('This line is not part of the loop body') # this line does not run in the for loop
print()  # Empty print line for space in the output

for i in (4, 2, 13):  # 3 items means loop runs 3x, even though they aren't 1,2,3
    print("Order doesn't matter:", i)

print()  # Empty print line for space in the output
# example 2
for x in range(10):  # creates 0,9
    print(x)

# example 3, loop over a list
for i in ['Hello', 'Goodbye,', 'How do you do,']:
    print(i, 'CS 1 students!')

# example 4, loop over a string
for letter in 'Hello CS Students! Welcome to class!':
    print(letter)  # Will print out each char one at a time, including spaces.

# example 5 Nested For Loops
# Nested loops are loops inside of loops. Loops within other loops are common. Generally you will not use more than
# 2 nested loops. I.e. "A loop inside of a loop inside of a loop" is the max.
# One "outer" for loop (a list of salutations)
# One "Inner" for loop (a list of names)
for i in ['Hello', 'Goodbye,', 'How do you do,']:
    for j in ['Kyle', 'Sachi', 'Russ', 'Alex']:
        print(i, j + '!')

