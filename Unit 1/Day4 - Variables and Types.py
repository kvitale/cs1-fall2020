# Scratch pad for day 4 8-30-2020


#Create some variables
variable1 = 3
variable2 = 4 #integer
someFloat = 2.334
someString = 'This that and the other' #spaces count as characters!
anotherString = 'Hello'

anExample = 3 + 4 + 4 * 2 / 5 #computes the right side and ASSIGNS the value to the variable on the left
print(anExample)

print(variable1 + variable2) #Print the result of adding two variables

var3 = variable1 + variable2 #Store the result of adding two variables
print(var3)
var3 = 45
print(var3)

myName = 'Kyle' #Example of good variable naming
print('Kyle') #Print a string
print(myName) #Print a variable, which contains a string


# How do you switch values of these variables?

a = 5
b = 6

#Can't do this!
b = a
a = b
