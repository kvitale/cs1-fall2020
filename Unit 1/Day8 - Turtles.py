#Day 8: Turtles, Scratchpad, For Loops, Range

# rectangle.py
import turtle
wn = turtle.Screen() #This creates the graphics window that turtles draw on
alex = turtle.Turtle() #This is a "turtle" object named alex.
meg = turtle.Turtle() #This is a "turtle" object named meg.

alex.forward(80) #This moves alex forward (whatever direction alex is facing) 80 pixels
alex.left(90) #This turns alex left, 90 degrees.
alex.forward(20)
alex.left(90)
alex.forward(80)
alex.left(90)
alex.forward(20)

# rectangle.py version 2

# This is the same as above, except repeated in a for loop 3 times.
# Keep in mind the 1,2,3 don't indicate the number of times the loop runs.
# Rather, the fact there are 3 numbers mean the loops runs 3x. You
# can substitute 4,8,2 or 0,100,0 for the 1,2,3 and achieve the same result.
for i in (1,2,3):
    alex.forward(80)
    alex.left(90)
    alex.forward(20)
    alex.left(90)
    alex.forward(80)
    alex.left(90)
    alex.forward(20)

wn.exitonclick() # The graphical window doesn't close until you click on it.

# The below for loop iterates over a list of 3 items.
# Each time the loop runs, the variable name changes to the next name in the list.
# The use of "name" as the variable is your choice.
for name in ["Sarah", "Chloe", "Tristan"]:
    print("Next up:", name)
    print("Good afternoon", name, "welcome to CS1.")

# Range Function ex.
#total = 0
x = 10
for i in range(x): #You can user variables in the range function, but they need to be integers.
    print('i is now:', i)
    #total = total + i

#print(total)

#What happens if you uncomment line 46 but not 42?
