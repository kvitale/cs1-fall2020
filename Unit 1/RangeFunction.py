# Range Function

# Example 0
print(list(range(10))) # create a list of 0-9. Range creates the numbers > converted to a list > then printed

# example 1
for i in [1, 2, 3, 4]:
    print(i)

for x in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:  # This could get long if I needed to do something 4000 times
    print(x)

for i in range(4):
    print('i is:', i)
    # Executes the body with i = 0, then 1, then 2, then 3
for x in range(400):  # Easy to read and maintain. Much easier than typing all numbers out
    print('X is:', x)
    # sets x to each of ... [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


# example 2
for j in range(10, 40, 5): #start, stop, step
    print('J is: ', j)



