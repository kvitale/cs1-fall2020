import pygame, sys
from pygame.locals import *
import random
pygame.init()

FPS = 30
fpsClock = pygame.time.Clock()

DISPLAYSURF = pygame.display.set_mode((600, 400), 0, 32)
pygame.display.set_caption('PRACTICE')

coin = pygame.mixer.Sound('pickup.wav')

#pygame.mixer.music.load('Rolemusic_-_Omou_matsu.mp3')
#pygame.mixer.music.play(-1, 0.0)

BLACK = (0,0,0)
WHITE=(255,255,255)
RED = (255,0,0)
BLUE = (0,0,255)
GREEN = (0,255,0)

DISPLAYSURF.fill(WHITE)

speed = 1

# Adding rectangle shapes
redX = 100
redY = 100

blueX = 100
blueY = 100

red = pygame.draw.rect(DISPLAYSURF, RED, (redX,redY,50,100))
redRect = pygame.Rect((redX,redY,50,100))

blue = pygame.draw.rect(DISPLAYSURF, BLUE, (blueX,blueY,50,100))
blueRect = pygame.Rect((blueX,blueY,50,100))

# Adding a cat image
#catImg = pygame.image.load('cat.png')
#catImgRect = catImg.get_rect()

fontObj = pygame.font.Font('freesansbold.ttf', 32)

textSurfaceObj = fontObj.render('Hello world!', True, GREEN, BLUE)
textRectObj = textSurfaceObj.get_rect()

textRectObj.center = (200, 150)

textSurfaceObj2 = fontObj.render('They are touching!', True, RED, BLUE)
textRectObj2 = textSurfaceObj2.get_rect()
textRectObj2.center = (400, 350)

score = 0
textSurfaceObj3 = fontObj.render(str(score), True, RED, BLUE)
textRectObj3 = textSurfaceObj2.get_rect()
textRectObj3.center = (550, 50)

touchingHelloWorld = False
showGreenRect = False

# run game loop
while True:
    DISPLAYSURF.fill(WHITE)

    DISPLAYSURF.blit(textSurfaceObj, textRectObj)

    textSurfaceObj3 = fontObj.render(str(score), True, RED, BLUE)
    DISPLAYSURF.blit(textSurfaceObj3, textRectObj3)

    #DISPLAYSURF.blit(catImg,catImgRect)
    #catImgRect.x += 1
    #catImgRect.y += 1

    if speed < 1:
        speed = 1

    if showGreenRect == True:
        pygame.draw.rect(DISPLAYSURF, GREEN, (300,300,50,50))

    if touchingHelloWorld == True:
        DISPLAYSURF.blit(textSurfaceObj2, textRectObj2)

    if redRect.colliderect(textRectObj) and blueRect.colliderect(textRectObj):
        touchingHelloWorld = True
        score += 1
    else:
        touchingHelloWorld = False

    pygame.draw.rect(DISPLAYSURF, RED, (redRect.x,redRect.y,50,100))
    pygame.draw.rect(DISPLAYSURF, BLUE, (blueRect.x,blueRect.y,50,100))

    redRect.x += random.randint(-5,0)

    if redRect.x < 0:
        redRect.x = 600

    blueRect.x += random.randint(0,5) + speed

    if blueRect.x > 600:
        blueRect.x = 0
        blueRect.y = random.randint(0,400)

    if redRect.colliderect(blueRect):
        score += 1
        showGreenRect = True
    else:
        showGreenRect = False



    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif (event.type == pygame.KEYDOWN):
            if (event.key == K_w):
                redRect.y -= 5
                speed += 1
                coin.play()
            elif (event.key == K_s):
                redRect.y += 5
                speed -= 1
                coin.play()
            elif (event.key == K_a):
                redRect.x -= 5
            elif (event.key == K_d):
                redRect.x += 5

    pygame.display.update()
    fpsClock.tick(FPS)















