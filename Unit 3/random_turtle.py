import turtle
import random

def is_in_screen(w,t):
    leftBound = - w.window_width() / 2
    rightBound = w.window_width() / 2
    topBound = w.window_height() / 2
    bottomBound = -w.window_height() / 2

    turtleX = t.xcor()
    turtleY = t.ycor()

    stillIn = True
    if turtleX > rightBound or turtleX < leftBound:
        stillIn = False
    if turtleY > topBound or turtleY < bottomBound:
        stillIn = False

    return stillIn

def main():
    t = turtle.Turtle()
    wn = turtle.Screen()

    t.shape('turtle')
    while is_in_screen(wn,t):
        coin = random.randrange(0, 2)
        if coin == 0:
            t.left(random.randrange(0, 181))
        else:
            t.right(random.randrange(0, 181))
        t.forward(50)

    wn.exitonclick()

main()