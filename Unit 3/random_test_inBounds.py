import turtle
import random
import math
t1 = turtle.Turtle()
t2 = turtle.Turtle()
t1.shape("turtle")
wn = turtle.Screen()

leftBound = -75
rightBound = 75
topBound = -75
bottomBound = 75
turtle.setworldcoordinates(leftBound, topBound, rightBound, bottomBound)

coin = 0 #flip a coin to determine if the turtle will turn left or right

def random_turtle_walk(t):
    while True:
        
        coin = random.randrange(0,2)
        rotate = random.randrange(0,361)
        heading = t.heading()
        print("heading : %i" % heading)
        x = t.xcor()
        y = t.ycor()
        if leftBound < x and x < rightBound and topBound < y and y < bottomBound :
            if coin == 1:
                newheading = (heading + rotate) % 360
            if coin == 0:
                newheading = (heading - rotate) % 360
        else:
            newheading = t.towards(0, 0)
            
        t.setheading(newheading)
        t.forward(25)

random_turtle_walk(t1)

wn.exitonclick()
