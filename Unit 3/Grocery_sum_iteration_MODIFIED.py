def checkout():
    total = 0
    count = 0
    moreItems = True
    while moreItems:
        price = float(input('Enter price of item (0 when done): '))
        if price > 0:
            count = count + 1
            total = total + price
            print('Subtotal: $ {0}'.format(total, 2))
        elif price < 0:
            print("Negative values are not allowed, try again.")
        else:
            moreItems = False
    if total > 0:
        average = total / count
    else:
        average = print("VOID, I cannot compute an average without data.")

    print('Total items: {0}'.format(count))
    print('Total $ {0:.2f}'.format(total))
    print('Average price per item: ${0:.2f}'.format(average))


checkout()
