# Import the cImage library
import cImage as image

# Add more functions, aka filters, here!

def trunc(num):
    """Trancates a number to between 0 and 255"""
    if num > 255:
        num = 255
    elif num < 0:
        num = 0
    return num

#def menu():
#    return choice

def addRed(img, value):
    '''Increases red value by value parameter'''
    # Get the RGB value of each pixel, and then overwrite it
    for row in range(img.getHeight()):
        for col in range(img.getWidth()):
            p = img.getPixel(col, row)      #Get the entire pixel, assign to variable p
            newred = p.getRed()             #Get the red pixel value from p
            newgreen = p.getGreen()         #Get the green pixel value from p
            newblue = p.getBlue()           #Get the blue pixel value from p
            #print(newred, newblue, newgreen) #test values
            newred = trunc(newred + value)  #Truncate the number to between 0 and 255
            newpixel = image.Pixel(newred, newgreen, newblue) #Create a new pixel using same values
            img.setPixel(col, row, newpixel) #Save the new pixel in the image

def adjusted_BW(img):
    '''Create B&W image'''
    # Get the RGB value of each pixel, and then overwrite it
    for row in range(img.getHeight()):
        for col in range(img.getWidth()):
            p = img.getPixel(col, row)      #Get the entire pixel, assign to variable p
            newred = p.getRed()             #Get the red pixel value from p
            newgreen = p.getGreen()         #Get the green pixel value from p
            newblue = p.getBlue()           #Get the blue pixel value from p
            #print(newred, newblue, newgreen) #test values
            color = int(trunc(newblue * .114 + newgreen * .587 + newred * .299))
            newpixel = image.Pixel(color, color, color) #Create a new pixel using same values
            img.setPixel(col, row, newpixel) #Save the new pixel in the image

# Your filters and menus(functions) should be added here!

def main():
    # Import an image, use any image you’d like
    # The image should be in the same folder as this python program
    img = image.Image("bridge.jpg")

    # Create a window for displaying the image. Takes 3 args, name, width and height
    win = image.ImageWin('Bridge',img.getWidth(),img.getHeight())

    # "Draw" the image on the window
    img.draw(win)

    #Get info from the user
    choice = menu()
    print("Option 1 \n"
          "Option 2 \n"
          "Option 3 \n"
          "etc"
          "")
    #addRed(img, 110) # adds 10 to red value, this is just an example
    if choice == 1:
        value = int(input('by how much?:'))
        addRed(img, value)
    elif choice == 2:
        adjusted_BW(img)

    # You will want to add your calls to functions here.
    #
    #

    # "ReDraw" the image on the window, after adding the above filter
    img.draw(win)

    # Wait for user to click on image to exit
    win.exitonclick()

main()


