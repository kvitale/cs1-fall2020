#11-3-2020
#if/elif/else practice

x = 50 #Practice value

#Part 0
if x >= 95:
    print('X is great or equal to 95 (part 0)')

#Part 1
if x > 90:
    print('X is greater than 90! (part 1)')
else:
    print('X is less than 90 (part 1)')

#Part 2
if x > 90:
    print('X is greater than 90! (part 2)')
elif x > 80:
    print('X is greater than 80... and? (part 2)')
elif x > 70:
    print('X is greater than 70... and? (part 2)')
else:
    print('X is less than 70.')


#Part 3
if x > 90:
    if x <= 100:
        print('You get an A')
    elif x > 100:
        print('You get an A+')
if x > 80:
    print('You get an B')
if x > 70:
   print('You get an C')
