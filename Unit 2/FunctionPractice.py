#10-6-2020

'''
#This is my function:

def makeMeASandwich (bread, cheese, condiments, option1, option2):
    add condiment to bread
    add cheese
    add option1
    add option2
    send sandwich back to person


#This is a call to my function:

makeMeASandwhich(wholeWheat, cheddar, mustard, avocado, turkey)
makeMeASandwhich(rye, swiss, ham, may, tomato, lettuce)


#We have seen some functions already
print()
type()
input()
int(), float(), list()
'''






import turtle

wn = turtle.Screen()
don = turtle.Turtle()

#This CREATES my function
def drawTriangle(myTurtle, size):
    '''Draws a triangle'''
    for i in range(3):
        myTurtle.forward(size)
        myTurtle.left(120)

#This CALLS my function
drawTriangle(don, 150)
don.forward(50)
don.color("RED")

#Calling the function a second time.
drawTriangle(don, 50)

#This is how to check the doc string in a function.
print(drawTriangle.__doc__)

wn.exitonclick()

