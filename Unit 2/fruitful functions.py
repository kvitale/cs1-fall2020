#10-13-2020
#Fruitful functions

def addTwoNumbers(x,y):
    '''This function adds two numbers'''
    c =  x+y
    return c

def multiplyTwoNumbers(x,y):
    '''This function takes two ints or floats and multiplies them'''
    #return toString(x*y)
    return (x*y)

def toString(data):
    '''This function converts data to a string and returns it'''
    return str(data)

def main():
    #Function calls
    answer = addTwoNumbers(5,3) #what's wrong with this?
    print(answer)
    answer = addTwoNumbers(23,6867)
    print(answer)

    multiplyTest = multiplyTwoNumbers(5,4)
    print(type(multiplyTest))

main()
