#This functionizes the quadratic function problem

'''
print("This program will determine the solutions to a quadratic equation in the form of ax^2+bx+c, based on the values you input.")

a = int(input("Please input a value for 'a': "))
b = int(input("Please input a value for 'b': "))
c = int(input("Please input a value for 'c': "))

x1 = (-b+((b**2)-(4*a*c))**0.5)/(2*a)
x2 = (-b-((b**2)-(4*a*c))**0.5)/(2*a)

print("The solutions to the quadratic equation with the values you entered are", x1, 'and', x2)
'''

A_Value = int(input("Please input a value for 'a': "))
B_Value = int(input("Please input a value for 'b': "))
C_Value = int(input("Please input a value for 'c': "))

def computerQuad(a,b,c):
    x1 = (-b+((b**2)-(4*a*c))**0.5)/(2*a)
    x2 = (-b-((b**2)-(4*a*c))**0.5)/(2*a)
    return x1, x2

ans1, ans2 = computerQuad(A_Value, B_Value, C_Value)
print(ans1)
print(ans2)
