import turtle

def drawTriangle(t, size):
    for i in range(3):
        t.forward(size)
        t.left(120)

def drawCircle(t, size):
    t.circle(size)

def drawRect(t, size):
    for i in range(4):
        t.forward(size)
        t.left(90)



def main():
    wn = turtle.Screen()
    leo = turtle.Turtle()

    leo.color('RED')
    drawTriangle(leo,50)

    leo.color('GREEN')
    drawRect(leo,75)

    wn.exitonclick()

main()
