#10/27/2020
#Booleans and IF Statements

# warmup function that takes a list as an argument and returns the first
# item in that list

#Warmup
def return_list ( myList ):
    return myList[0]

#Practice example
x= 50

if x % 2 == 0:
    print(x, "is even.")
    print("Did you know that 2 is the only even number that is prime?")

elif x % 3 == 0:
    print('x is visiible by 3!')

else:
    print(x, "is odd.")
    print("Did you know that multiplying two odd numbers gives an odd result?")
